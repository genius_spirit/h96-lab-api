import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import Cocktails from "./containers/Cocktails/Cocktails";
import Login from "./containers/Login/Login";
import AboutCocktail from "./containers/AboutCocktail/AboutCocktail";
import AddNewCocktail from "./containers/AddNewCocktail/AddNewCocktail";

const Routes = ({ user }) => {
  return (
    <Switch>
      <Route path="/" exact  component={Cocktails}/>
      <Route path="/login" exact  component={Login}/>
      <Route path="/cocktails/about/:id" exact component={AboutCocktail}/>
      <Route path="/add-new-cocktail" exact component={AddNewCocktail}/>
      <Route
        render={() => <h1 style={{ textAlign: "center" }}>Page not found</h1>}
      />
    </Switch>
  );
};

const mapStateToProps = state => {
  return {
    user: state.users.user
  };
};

export default withRouter(connect(mapStateToProps)(Routes));
