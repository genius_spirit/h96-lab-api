import {
  ABOUT_COCKTAIL_FAILURE,
  ABOUT_COCKTAIL_SUCCESS,
  GET_COCKTAILS_FAILURE,
  GET_COCKTAILS_SUCCESS, SEND_RATING_FAILURE
} from "../actions/actionTypes";

const initialstate = {
  cocktails: [],
  aboutCocktail: [],
  error: null,
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case GET_COCKTAILS_SUCCESS:
      return {...state, cocktails: action.cocktails};
    case GET_COCKTAILS_FAILURE:
      return {...state, error: action.error};
    case ABOUT_COCKTAIL_SUCCESS:
      return {...state, aboutCocktail: action.data};
    case ABOUT_COCKTAIL_FAILURE:
      return {...state, error: action.error};
    case SEND_RATING_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;