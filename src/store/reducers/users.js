import {
  FACEBOOK_LOGIN_FAILURE,
  FACEBOOK_LOGIN_SUCCESS,
  LOGOUT_USER,

} from "../actions/actionTypes";

const initialState = {
  loginError: null,
  user: null,
  token: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FACEBOOK_LOGIN_SUCCESS:
      return {...state, user: action.user, loginError: null, token: action.token};
    case FACEBOOK_LOGIN_FAILURE:
      return {...state, loginError: action.error};
    case LOGOUT_USER:
      return {...state, user: null};
    default:
      return state;
  }
};

export default reducer;