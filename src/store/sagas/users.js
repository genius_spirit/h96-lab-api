import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import { push } from "react-router-redux";
import { NotificationManager } from "react-notifications";
import {
  facebookLoginSuccess,
  facebookLoginFailure,
  logoutUserSuccess,
  logoutExpiredUserSuccess
} from "../actions/users";

export function* facebookLoginSaga(action) {
  try {
    const response = yield axios.post('/users/facebookLogin', action.data);
    if (response) {
      yield put(facebookLoginSuccess(response.data.user, response.data.token));
      NotificationManager.success('Success', "Log In with Facebook");
      yield put(push("/"));
    }
  } catch (error) {
    yield put(facebookLoginFailure(error.response.data));
  }
}

export function* logoutUserSaga() {
  try {
    yield axios.delete("/users/sessions");
    yield put(logoutUserSuccess());
    yield put(push("/"));
    NotificationManager.success("Success", "Successfully LogOut");
  } catch (e) {
    NotificationManager.error("Error", "Logout failed");
  }
}

export function* logoutExpiredUserSaga() {
  try {
    yield put(logoutExpiredUserSuccess());
    yield put(push("/"));
    NotificationManager.error("Error", "Expired time of session");
  } catch (error) {
    console.log("logout expired user error:________", error);
  }
}

