import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import { NotificationManager } from "react-notifications";

import {
  aboutCocktailFailure,
  aboutCocktailSuccess,
  getCocktailsFailure,
  getCocktailsSuccess, sendRatingFailure, sendRatingSuccess
} from "../actions/cocktails";

export function* getCocktailsSaga() {
  try {
    const response = yield axios.get('/cocktails');
    if (response) {
      yield put(getCocktailsSuccess(response.data));
    }
  } catch (error) {
    yield put(getCocktailsFailure(error.response.data));
  }
}

export function* aboutCocktailSaga(action) {
  try {
    const response = yield axios.get(`/cocktails/about/${action.id}`);
    if (response) {
      yield put(aboutCocktailSuccess(response.data));
    }
  } catch (error) {
    yield put(aboutCocktailFailure(error.response.data));
  }
}

export function* sendRatingSaga(action) {
  try {
    const rate = yield axios.post('/cocktails/rating', action.rateData);
    if (rate) {
      yield put(sendRatingSuccess);
      NotificationManager.info(rate.data.message);
    }
  } catch (error) {
    yield put(sendRatingFailure(error.response.data));
  }
}