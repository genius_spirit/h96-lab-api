import {takeEvery, all} from 'redux-saga/effects';
import {
  ABOUT_COCKTAIL,
  FACEBOOK_LOGIN,
  GET_COCKTAILS,
  LOGOUT_USER,
  LOGOUT_USER_EXPIRED, SEND_RATING
} from "../actions/actionTypes";
import { facebookLoginSaga, logoutExpiredUserSaga, logoutUserSaga } from "./users";
import { aboutCocktailSaga, getCocktailsSaga, sendRatingSaga } from "./cocktails";

function* facebookLogin() {
  yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
}

function* getCocktails() {
  yield takeEvery(GET_COCKTAILS, getCocktailsSaga);
}

function* logoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
}

function* logoutExpiredUser() {
  yield takeEvery(LOGOUT_USER_EXPIRED, logoutExpiredUserSaga);
}

function* aboutCocktail() {
  yield takeEvery(ABOUT_COCKTAIL, aboutCocktailSaga)
}

function* sendRating() {
  yield takeEvery(SEND_RATING, sendRatingSaga);
}

export function* rootSaga() {
  yield all([
    facebookLogin(),
    logoutUser(),
    logoutExpiredUser(),
    getCocktails(),
    aboutCocktail(),
    sendRating(),

  ])
}