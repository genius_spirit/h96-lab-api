
export const FACEBOOK_LOGIN_SUCCESS = 'FACEBOOK_LOGIN_SUCCESS';
export const FACEBOOK_LOGIN_FAILURE = 'FACEBOOK_LOGIN_FAILURE';
export const FACEBOOK_LOGIN = 'FACEBOOK_LOGIN';

export const LOGOUT_USER = 'LOGOUT_USER';
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS';
export const LOGOUT_USER_EXPIRED = 'LOGOUT_USER_EXPIRED';

export const GET_COCKTAILS_SUCCESS = 'GET_COCKTAILS_SUCCESS';
export const GET_COCKTAILS_FAILURE = 'GET_COCKTAILS_FAILURE';
export const GET_COCKTAILS = 'GET_COCKTAILS';

export const ABOUT_COCKTAIL = 'ABOUT_COCKTAIL';
export const ABOUT_COCKTAIL_SUCCESS = 'ABOUT_COCKTAIL_SUCCESS';
export const ABOUT_COCKTAIL_FAILURE = 'ABOUT_COCKTAIL_FAILURE';

export const SEND_RATING = 'SEND_RATING';
export const SEND_RATING_SUCCESS = 'SEND_RATING_SUCCESS';
export const SEND_RATING_FAILURE = 'SEND_RATING_FAILURE';