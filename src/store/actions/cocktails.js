import {
  ABOUT_COCKTAIL, ABOUT_COCKTAIL_FAILURE,
  ABOUT_COCKTAIL_SUCCESS,
  GET_COCKTAILS,
  GET_COCKTAILS_FAILURE,
  GET_COCKTAILS_SUCCESS, SEND_RATING, SEND_RATING_FAILURE, SEND_RATING_SUCCESS
} from "./actionTypes";

export const getCocktailsSuccess = cocktails => {
  return {type: GET_COCKTAILS_SUCCESS, cocktails};
};

export const getCocktailsFailure = error => {
  return {type: GET_COCKTAILS_FAILURE, error};
};

export const getCocktails = () => {
  return {type: GET_COCKTAILS};
};

export const aboutCocktail = (id) => {
  return { type: ABOUT_COCKTAIL, id};
};

export const aboutCocktailSuccess = (data) => {
  return {type: ABOUT_COCKTAIL_SUCCESS, data};
};

export const aboutCocktailFailure = (error) => {
  return {type: ABOUT_COCKTAIL_FAILURE, error};
};

export const sendRating = rateData => {
  return {type: SEND_RATING, rateData};
};

export const sendRatingSuccess = () => {
  return {type: SEND_RATING_SUCCESS};
};

export const sendRatingFailure = error => {
  return {type: SEND_RATING_FAILURE, error}
};

