import React from "react";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const Toolbar = ({ user, logout }) => {

  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <LinkContainer to="/" exact>
            <a>Cocktail Builder</a>
          </LinkContainer>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        {user ? (
          <Nav pullRight activeKey={1}>
            <LinkContainer to="/add-new-cocktail" exact>
              <NavItem>Add new cocktail</NavItem>
            </LinkContainer>
            <NavItem eventKey={3} onClick={logout}>LOGOUT</NavItem>
          </Nav>
        ) : (
          <Nav pullRight activeKey={1}>
            <LinkContainer to="/login" exact>
              <NavItem>Login</NavItem>
            </LinkContainer>
          </Nav>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Toolbar;
