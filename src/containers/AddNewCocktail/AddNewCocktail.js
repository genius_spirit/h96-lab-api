import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Header } from "semantic-ui-react";
import { Button, Col, ControlLabel, Form, FormGroup, Row } from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement/FormElement";

class AddNewCocktail extends Component {

  state = {
    name: '',
    recipe: '',
    ingredients: [{ingredientName: '', count: ''}],
    image: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  ingredientChangeHandler = (event, index) => {
    const ingredients = [...this.state.ingredients];
    ingredients[index] = {...ingredients[index], [event.target.name]: event.target.value};
    this.setState({ingredients})
  };


  addNewFieldHandler = () => {
    this.setState({ ingredients: [...this.state.ingredients, {ingredientName: '', count: ''} ]});
  };

  render() {
    return (
      <Grid container>
        <Grid.Column width={16}>
          <Header as="h2" dividing>
            Add new cocktail
          </Header>
          <Form horizontal onSubmit={this.submitFormHandler}>

            <FormElement
              propertyName="name"
              title="Name"
              type="text"
              value={this.state.name}
              changeHandler={this.inputChangeHandler}
              required
            />

            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                Ingredients:
              </Col>
              <Col sm={10}>

                  {this.state.ingredients &&
                  this.state.ingredients.map((item, index) => (
                    <Row key={index}>
                      <Col md={6}>
                        <FormElement
                          propertyName="ingredientName"
                          title="name"
                          type="text"
                          value={item.ingredientName}
                          changeHandler={event =>this.ingredientChangeHandler(event, index)}
                          required
                        />
                      </Col>
                      <Col md={6}>
                        <FormElement
                          propertyName="count"
                          title="count"
                          type="text"
                          value={item.count}
                          changeHandler={event => this.ingredientChangeHandler(event, index)}
                          required
                        />
                      </Col>
                    </Row>
                  ))
                  }
                  <Button onClick={this.addNewFieldHandler}>Add ingredient</Button>

              </Col>
            </FormGroup>

            <FormElement
              propertyName="recipe"
              title="Recipe"
              type="textarea"
              value={this.state.recipe}
              changeHandler={this.inputChangeHandler}
              required
            />

            <FormElement
              propertyName="image"
              title="Image"
              type="file"
              changeHandler={this.fileChangeHandler}
            />

            <FormGroup>
              <Col smOffset={2} sm={10}>
                <Button bsStyle="primary" type="submit">Add</Button>
              </Col>
            </FormGroup>

          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};


export default connect(mapStateToProps, mapDispatchToProps)(AddNewCocktail);
