import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Header, Image, List, Rating } from "semantic-ui-react";
import { aboutCocktail, sendRating } from "../../store/actions/cocktails";
import config from "../../config";
import { NotificationManager } from "react-notifications";

class AboutCocktail extends Component {
  state = { rating: null };

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.aboutCocktail(id);
  }

  handleRate = async (e, { rating }) => {
    await this.setState({ rating });
    if (this.props.user) {
      const rateData = {
        id: this.props.cocktail._id,
        userId: this.props.user._id,
        rating: this.state.rating
      };
      this.props.sendRating(rateData);
    } else {
      NotificationManager.error("Need to Login before add rating");
    }
  };

  render() {
    return (
      <Grid container>
        <Grid.Column width={6}>
          <Image
            src={
              this.props.cocktail.image &&
              config.apiUrl + "/uploads/" + this.props.cocktail.image
            }
          />
        </Grid.Column>
        <Grid.Column width={10}>
          <Header as="h2" dividing>
            {this.props.cocktail.title}
          </Header>
          <h4>Ingredients:</h4>
          <List bulleted>
            {this.props.cocktail.ingredients &&
              this.props.cocktail.ingredients.map(item => (
                <List.Item key={item._id}>
                  {item.count + " - " + item.name}
                </List.Item>
              ))}
          </List>
        </Grid.Column>
        <Grid.Column width={16}>
          <Header as="h3" dividing>
            Recipe:
          </Header>
          <p>{this.props.cocktail.recipe}</p>
          <Header as='h3' dividing>
            Add rate:
          </Header>
          <Rating
            maxRating={10}
            icon="star"
            size="huge"
            onRate={this.handleRate}
          />
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    cocktail: state.cocktails.aboutCocktail,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    aboutCocktail: id => dispatch(aboutCocktail(id)),
    sendRating: rateData => dispatch(sendRating(rateData))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutCocktail);
