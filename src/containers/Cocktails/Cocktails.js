import React, { Component } from "react";
import { connect } from "react-redux";
import { getCocktails } from "../../store/actions/cocktails";
import config from "../../config";
import { Card, Grid } from "semantic-ui-react";

class Cocktails extends Component {
  componentDidMount() {
    this.props.getCocktails();
  }

  render() {
    return (
      <Grid container columns={4}>
        {this.props.cocktails &&
        this.props.cocktails.map(item => (
          <Grid.Column key={item._id}>
            <Card
              href={`/cocktails/about/${item._id}`}
              image={config.apiUrl + '/uploads/' + item.image}
              header={item.title}
            />
          </Grid.Column>
        ))
        }
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    cocktails: state.cocktails.cocktails
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCocktails: cocktails => dispatch(getCocktails(cocktails))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);
